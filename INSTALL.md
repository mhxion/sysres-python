INSTALLATION
============


## Prerequisite Library Installation 

Simply download the files to your system, and then install the `psutil` module 
from PyPI in your environment.

This requires you have PIP available in your environment.  Once the `psutil` 
module is installed, you have all the prerequisites.


## Using `sysres.py`

Simply run `sysres.py` directly, or with `python3` on your system.

That's all there is to it.

It provides something like this as output:

```
System: system.example.com
Date/Time: 2020-02-10 15:48:33
Load Avgs (1m, 5m, 15m): (1.65, 1.04, 1.08)
CPU Usage: 12.8
RAM Usage: 9.700GiB / 31.042GiB  (31%)
Swap Usage: 0.000B / 4.000GiB  (0%)
```


## Using `sysres-email.py` for email alerts

Bit more you have to do here.

Edit the file, and look at the SMTP_ variables.  Update them for your SMTP 
servers.  Also update NOTIFY_ variables for your environment.

Choose what tests and thresholds you want to trigger notifications for and at 
what thresholds.

Then just run it.  You will get no output, but you will also not get any emails unless 
you set alert limits and those are exceeded.