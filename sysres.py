#!/usr/bin/env python3

import psutil
import datetime
import socket
import sys
import time

# DO NOT ALTER - this is a Python version check to make sure that we are using new enough Python!
if sys.version_info.major < 3 and sys.version_info.minor < 6:
    raise EnvironmentError("You can only use this script with Python 3.6 or newer!")

PRINT_OUT = """System: {system}
Date/Time: {dt}
Load Avgs (1m, 5m, 15m): {loadavg}
CPU Usage: {cpu_perc}
RAM Usage: {ram_used} / {ram_avail}  ({ram_perc}%)
Swap Usage: {swap_used} / {swap_avail}  ({swap_perc}%)"""


# CPU tests
CPU_TEST = True  # Default is enable CPU warning case.
CPU_USAGE_THRESHOLD = 75  # Integer - Percent usage threshold to trigger a CPU alarm.

# RAM Tests
RAM_TEST = True  # Default is to enable RAM usage warning case.
RAM_USAGE_THRESHOLD = 90  # Integer - Percent RAM usage to trigger a RAM alarm at.

# SWAP Tests
SWAP_TEST = True
SWAP_USAGE_THRESHOLD = 50  # Integer representing percent SWAP usage to trigger a SWAP alarm at.


def _human_readable_size(size: int, decimal_places: int = 3) -> str:
    for unit in ['B', 'KiB', 'MiB', 'GiB', 'TiB']:
        if size < 1024.0:
            break
        size /= 1024.0
    return f"{size:.{decimal_places}f}{unit}"


def _print(load, cpu_perc: float, ram_used: int, ram_avail: int,
           swap_used: int, swap_avail: int):
    print(
        PRINT_OUT.format(
            system=socket.gethostname(),
            dt=datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
            loadavg=load,
            cpu_perc=cpu_perc,
            ram_used=_human_readable_size(ram_used),
            ram_avail=_human_readable_size(ram_avail),
            ram_perc=int(float(ram_used / ram_avail) * 100),
            swap_used=_human_readable_size(swap_used),
            swap_avail=_human_readable_size(swap_avail),
            swap_perc=int(float(swap_used / swap_avail) * 100),
        )
    )


def _get_resources() -> None:
    loadavg = psutil.getloadavg()
    time.sleep(0.5)  # Needed to get accurate CPU usage reading.
    cpu_perc = psutil.cpu_percent()
    ram_used = psutil.virtual_memory().used
    ram_avail = psutil.virtual_memory().total
    swap_used = psutil.swap_memory().used
    swap_avail = psutil.swap_memory().total

    # Alarms
    cpu_alarm = False
    ram_alarm = False
    swap_alarm = False

    alarmstr = ""

    # CPU threshold alarm when CPU Usage is over 70%
    if CPU_TEST and (cpu_perc >= CPU_USAGE_THRESHOLD):
        cpu_alarm = True
        alarmstr += f"Current CPU usage is above {CPU_USAGE_THRESHOLD}%!\n"

    # RAM usage on system is over 90% of available RAM
    if RAM_TEST and ((ram_used / ram_avail) * 100) >= RAM_USAGE_THRESHOLD:
        ram_alarm = True
        alarmstr += f"Current RAM consumption is over {RAM_USAGE_THRESHOLD}% of available RAM!\n"

    # Swap has to be existing on system, and total swap usage is over 50%
    if SWAP_TEST and swap_avail != 0 and ((swap_used / swap_avail) * 100) >= SWAP_USAGE_THRESHOLD:
        swap_alarm = True
        alarmstr += f"Current SWAP consumption is over {SWAP_USAGE_THRESHOLD}% of total Swap Space!"

    if cpu_alarm or ram_alarm or swap_alarm:
        pass
        # _send_email(alarmstr, loadavg, cpu_perc, ram_used, ram_avail, swap_used, swap_avail)

    _print(loadavg, cpu_perc, ram_used, ram_avail, swap_used, swap_avail)


if __name__ == "__main__":
    _get_resources()
