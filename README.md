This is a fairly simple program, but it utilizes the `psutil` library 
and specifically provides the ability to quickly get data out from the system 
about resource usage and prints it to the command line (stdout).  There is also an email alert 
variant.

This code is released under the MIT License and is copywriten under this license.  See the 
LICENSE file for details.

Copyright (C) 2020 by Thomas Ward <teward@ubuntu.com>


There are two variants of the script - `sysres.py` which simply spits out to standard out.

The second is `sysres-email.py` and needs configured to be utilized, as it has test thresholds, 
test enablement, and alert case conditions which can be used to determine whether or not to send 
an alert email to a destination address based on CPU usage, RAM usage, or SWAP usage.  While it 
also includes load averages, there is no mechanism configured to actually *test* load averages and 
generate alerts based off load averages (though there may be in the near future) 